import restaurant
import reservation
import customer


def simple_runtime_test(therest):
    bob = customer.Customer("Bob", "123456789", [])
    jane = customer.Customer("Jane", "87654323", [])
    therest.add_customer(bob)
    therest.add_customer(jane)

    res1 = reservation.Reservation(bob, "Monday", "3:00", 2)
    res2 = reservation.Reservation(jane, "Thursday", "1:00", 4)

    therest.make_reservation(res1)
    therest.make_reservation(res2)

    existing_user = therest.existing_customer(jane)
    if existing_user:
        user_res = therest.find_reservation_by_name(jane)
        if user_res is not None:
            therest.cancel_reservation(res2)
        else:
            print("Reservation not found")
    else:
        print("Customer does not exist")

    joe = customer.Customer("Joe", "876593323", [])
    therest.add_customer(joe)
    res3 = reservation.Reservation(joe, "Monday", "6:00", 1)
    therest.make_reservation(res3)

    res2.set_booking(res2.booking.date, '1:00')
    therest.make_reservation(res2)

    monday_customers = therest.find_customers_by_day("Monday")
    if len(monday_customers) == 2:
        print("\nMondays customers:")
        for item in monday_customers:
            print(item.to_string())

    print("\nReservations: ")
    therest.print_reservations()


def main():
    myrest = restaurant.Restaurant().get()
    simple_runtime_test(myrest)


main()
