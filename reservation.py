import booking

class Reservation:
    def __init__(self, customer, date, time, size):
        self.booking = booking.Booking(date, time)
        self.customer = customer
        self.size = size

    def to_string(self):
        s = self.customer.name + ', ' + self.booking.date + ', ' + self.booking.time + ', ' + str(self.size)
        return s

    def get_booking(self):
        return self.booking

    def set_booking(self, date, time):
        self.booking = booking.Booking(date, time)

    def get_customer(self):
        return self.customer

    def set_size(self, size):
        self.size = size

    def get_size(self):
        return self.size
