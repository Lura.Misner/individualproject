# IndividualProject

This project is to simulate a restaurant reservation system,
there are classes for Customers, Reservations, Bookings, and the Restaurant. 

## Customer.py

Represents each individual customer, with a name, phone number, and a list of all their reservations.

## Booking.py

Represent a a block of time on the restaurants schedule.

## Reservation.py

Creates each reservation, takes a customer, date, time, and a size of the party. Uses the date
and time to create the booking.

## Restaurant.py

Represents the actual restaurant. Has a list of customers, a list of reservations, and a schedule. The schedule is 
uploaded from schedule.txt and populates a list. 
Responsible for making and canceling reservations, looking up reservations, looking up customers, checking table 
availability count for a specific date and time, etc. 