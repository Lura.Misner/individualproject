import unittest
import restaurant
import reservation
import customer


class TestCheckout(unittest.TestCase):
    restaurant = None

    @classmethod
    def setUpClass(cls):
        # called one time, at the very beginning
        print('setUpClass()')
        cls.restaurant = restaurant.Restaurant().get()

        # Make a few customers
        cls.bob = customer.Customer("Bob", "123456789", [])
        cls.jane = customer.Customer("Jane", "87654323", [])
        cls.joe = customer.Customer("Joe", "876593323", [])

        # Make a few reservations
        cls.res_1 = reservation.Reservation(cls.jane, 'Monday', '5:00', 2)
        cls.res_2 = reservation.Reservation(cls.jane, 'Friday', '12:00', 1)
        cls.res_3 = reservation.Reservation(cls.joe, 'Thursday', '11:00', 3)
        cls.res_4 = reservation.Reservation(cls.bob, 'Friday', '6:00', 2)

        # Add customers to the restaurant
        cls.restaurant.add_customer(cls.bob)
        cls.restaurant.add_customer(cls.jane)
        cls.restaurant.add_customer(cls.joe)

    @classmethod
    def tearDownClass(cls):
        # called one time, at the very end--if you need to do any final cleanup, do it here
        print('tearDownClass()')

    def setUp(self):
        # called before every test
        print('setUp()')

    def tearDown(self):
        # called after every test
        self.restaurant.clear_reservations()
        print('tearDown()')

    # -------------------------------------------------------------
    def test_return_incorrect(self):
        # This tests the incorrect version of cancel_reservation of restaurant: this test will fail
        # Make the reservation, and record the current tables
        table_before = self.restaurant.get_table_count(self.res_1.booking.date, self.res_1.booking.time)
        t1 = self.restaurant.make_reservation(self.res_1)
        self.assertTrue(t1)

        # Cancel (incorrect) reservation, and record the current tables
        removed = self.restaurant.incorrect_cancel_reservation(self.res_1)
        if removed:
            table_after = self.restaurant.get_table_count(self.res_1.booking.date, self.res_1.booking.time)
        else:
            table_after = 0

        # When the reservation is made, the table count for the table is decreased by 1.
        # This will fail, because in my code I did not re-add the table after the reservation was canceled
        self.assertEqual(table_before, table_after)

    # -------------------------------------------------------------
    def test_return_correct(self):
        # This tests the correct version of cancel_reservation of restaurant: this test will pass
        # Make the reservation, and record the current tables
        table_before = self.restaurant.get_table_count(self.res_1.booking.date, self.res_1.booking.time)
        t1 = self.restaurant.make_reservation(self.res_1)
        self.assertTrue(t1)

        # Cancel (correct) reservation, and record the current tables
        removed = self.restaurant.cancel_reservation(self.res_1)
        if removed:
            table_after = self.restaurant.get_table_count(self.res_1.booking.date, self.res_1.booking.time)
        else:
            table_after = 0

        # When the reservation is made, the table count for the table is decreased by 1.
        # This will pass because it is re-added in the cancel_reservation
        self.assertEqual(table_before, table_after)

    # -------------------------------------------------------------
    def test_reservation_one(self):
        # Make Janes reservations, should return true for successful
        t1 = self.restaurant.make_reservation(self.res_1)
        t2 = self.restaurant.make_reservation(self.res_2)
        self.assertTrue(t1)
        self.assertTrue(t2)

        # Make one by someone else, to confirm that it isnt just returning all reservations
        t3 = self.restaurant.make_reservation(self.res_4)
        self.assertTrue(t3)

        # Find reservation by name for Jane
        res_list = self.restaurant.find_reservation_by_name(self.jane)

        # Should have both of her reservations, len(result) == 2
        self.assertEqual(len(res_list), 2)

    # -------------------------------------------------------------
    def test_reservation_two(self):
        # Make Jane and Bobs Friday reservations, should return true for successful
        t1 = self.restaurant.make_reservation(self.res_2)
        t2 = self.restaurant.make_reservation(self.res_4)
        self.assertTrue(t1)
        self.assertTrue(t2)

        # Find Customer by day, length of what is returned should be 2
        cust_list = self.restaurant.find_customers_by_day("Friday")
        self.assertEqual(len(cust_list), 2)

    # -------------------------------------------------------------
    def test_reservation_three(self):
        # Make Joes reservation, picked a day and time with no available tables
        test = self.restaurant.make_reservation(self.res_3)

        # it should return false because there are no tables that day
        self.assertFalse(test)

    # -------------------------------------------------------------
    def test_reservation_four(self):
        # Number of reservations before should be 0
        num_res_before = len(self.jane.get_reservations())
        self.assertEqual(num_res_before, 0)

        # Make the reservations
        t1 = self.restaurant.make_reservation(self.res_1)
        t2 = self.restaurant.make_reservation(self.res_2)
        self.assertTrue(t1)
        self.assertTrue(t2)

        # Number of reservations before should be 2
        num_res_after = len(self.jane.get_reservations())
        self.assertEqual(num_res_after, 2)

    # -------------------------------------------------------------
    def test_reservation_five(self):

        # Reservations at the start should be 0
        num_res_start = len(self.restaurant.get_reservations())
        self.assertEqual(num_res_start, 0)

        # Make reservations, should return true for successfully scheduling
        t1 = self.restaurant.make_reservation(self.res_2)
        t2 = self.restaurant.make_reservation(self.res_4)
        self.assertTrue(t1)
        self.assertTrue(t2)

        # Reservations should have a length of 2
        num_res_mid = len(self.restaurant.get_reservations())
        self.assertEqual(num_res_mid, 2)

        # Remove a reservation, should have a length of 1
        removed = self.restaurant.cancel_reservation(self.res_2)
        self.assertTrue(removed)

        num_res_after = len(self.restaurant.get_reservations())
        self.assertEqual(num_res_after, 1)


if __name__ == '__main__':
    unittest.main()
