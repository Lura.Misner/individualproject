
class Booking:
    def __init__(self, date, time):
        self.date = date
        self.time = time

    def get_date(self):
        return self.date

    def get_time(self):
        return self.time

    def set_date(self, date):
        self.date = date

    def set_time(self, time):
        self.time = time
