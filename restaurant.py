

class Restaurant:
    res = None

    @classmethod
    def get(self):
        if self.res is None:
            self.res = Restaurant()
        return self.res

    def __init__(self):
        self.customers = list()
        self.reservations = list()
        # Populate schedule Dict[day] = List[Tuple (time, tables)]
        schedule_dict = dict()

        # Lists for each day
        monday = list()
        tuesday = list()
        wednesday = list()
        thursday = list()
        friday = list()
        try:
            # Open file
            file = open("schedule.txt", "r")

            # Skip first line, its headers
            line = file.readline()
            line = file.readline()
            while line != '':
                line.strip('\n')
                words = line.split()

                # Sort each line into these three
                day = words[0].strip(",")
                time = words[1].strip(",")
                tables = words[2].strip(",")

                # Add them to the lists
                if day == "Monday":
                    monday.append((time, int(tables)))
                elif day == "Tuesday":
                    tuesday.append((time, int(tables)))
                elif day == "Wednesday":
                    wednesday.append((time, int(tables)))
                elif day == "Thursday":
                    thursday.append((time, int(tables)))
                elif day == "Friday":
                    friday.append((time, int(tables)))

                line = file.readline()

            file.close()
        # Throw exception
        except IOError:
            print("Problem with file.")

        # Add the lists to the dictionary
        schedule_dict["Monday"] = monday
        schedule_dict["Tuesday"] = tuesday
        schedule_dict["Wednesday"] = wednesday
        schedule_dict["Thursday"] = thursday
        schedule_dict["Friday"] = friday
        self.schedule = schedule_dict

    def add_customer(self, customer):
        self.customers.append(customer)

    def get_customers(self):
        return self.customers

    def existing_customer(self, customer):
        if customer in self.customers:
            return True
        else:
            return False

    def list_schedule(self):
        return self.schedule

    def make_reservation(self, reservation):
        booking = reservation.get_booking()
        day = booking.get_date()
        time = booking.get_time()
        customer = reservation.get_customer()

        list_tuples = self.schedule[day]
        tables = -1
        index = 0
        seen = False
        for item in list_tuples:
            if item[0] == time:
                tables = item[1]
                seen = True
            if not seen:
                index = index + 1

        # Check that theres a table available
        if tables > 0:
            self.reservations.append(reservation)
            # Subtract one from tables available
            self.schedule[day][index] = (time, (tables-1))
            print("Reservation made for " + customer.get_name())
            customer.add_reservation(reservation)
            return True

        else:
            print("No table spots are available")
            return False

    def incorrect_cancel_reservation(self, reserv):
        # This is wrong because I am not re adding the table after the reservation was canceled
        if reserv in self.reservations:
            self.reservations.remove(reserv)
            print("Reservation removed for " + reserv.get_customer().get_name() + "!")

            # Remove from the customers list
            customer = reserv.get_customer()
            customer.remove_reservation(reserv)
            return True
        else:
            print("Reservation not found, could not remove")
            return False

    def cancel_reservation(self, reserv):
        if reserv in self.reservations:
            self.reservations.remove(reserv)
            print("Reservation removed for " + reserv.get_customer().get_name() + "!")

            # Add back to table
            booking = reserv.get_booking()
            day = booking.get_date()
            time = booking.get_time()

            list_tuples = self.schedule[day]
            tables = -1
            index = 0
            seen = False
            for item in list_tuples:
                if item[0] == time:
                    tables = item[1]
                    seen = True
                if not seen:
                    index = index + 1

            self.schedule[day][index] = (time, (tables + 1))

            # Remove from the customers list
            customer = reserv.get_customer()
            customer.remove_reservation(reserv)

            return True
        else:
            print("Reservation not found, could not remove")
            return False

    def find_reservation_by_name(self, customer):
        res_list = list()
        for res in self.reservations:
            if res.get_customer() == customer:
                res_list.append(res)

        if len(res_list) > 0:
            return res_list
        else:
            return None

    def find_customers_by_day(self, day):
        res_list = list()
        for res in self.reservations:
            if day == res.get_booking().get_date():
                res_list.append(res.customer)

        if len(res_list) != 0:
            return res_list
        else:
            return None

    def print_reservations(self):
        for item in self.reservations:
            print(item.to_string())

    def get_table_count(self, day, time):
        list_tuples = self.schedule[day]
        tables = -1
        for item in list_tuples:
            if item[0] == time:
                tables = item[1]

        if tables > 0:
            return tables
        else:
            return None

    def clear_reservations(self):
        self.__init__()

    def get_reservations(self):
        return self.reservations
