
class Customer:
    def __init__(self, name, phone_number, reservations):
        self.name = name
        self.phone_number = phone_number
        self.reservations = reservations

    def to_string(self):
        s = self.name + ', ' + self.phone_number + ', ' + str(len(self.reservations)) + ' Reservation(s) '
        return s

    def __eq__(self, other):
        return self.name == other.name and self.phone_number == other.phone_number

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_reservations(self):
        return self.reservations

    def add_reservation(self, reserv):
        self.reservations.append(reserv)

    def remove_reservation(self, reserv):
        if reserv in self.reservations:
            self.reservations.remove(reserv)

    def get_phone(self):
        return self.phone_number

    def set_phone(self, phone):
        self.phone_number = phone
